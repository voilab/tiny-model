/*jslint browser: true */
/*global define */
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['angular'], factory);
    } else {
        // Browser globals
        root.amdWeb = factory(root.angular);
    }
}(this, function (angular) {

    'use strict';

    angular.module('model', []).provider('Model', function () {
        var
            /**
             * Création d'un model avec les données de base appliquées et traitement des relations
             *
             * @param  {Object} Model Constructeur du model
             * @param  {Object} base  Données de base
             * @return {Object}       Instance du model
             */
            createOne = function (Model, base) {
                var instance = new Model(base);

                angular.forEach(Model.relations, function (Model, property) {
                    var instances = [];

                    if (this[property] !== undefined) {
                        if (angular.isArray(this[property])) {
                            angular.forEach(this[property], function (relation_base) {
                                instances.push(
                                    createOne(Model, relation_base)
                                );
                            }, this);

                            this[property] = instances;
                        } else {
                            this[property] = createOne(Model, this[property]);
                        }
                    }
                }, instance);

                return instance;
            },

            /**
             * Traitement d'une collection de models à instancier
             *
             * @param  {Object} Model Constructeur du model
             * @param  {Array}  bases Liste des données de bases
             * @return {Array}        Tableau d'instances créées sur les données de base
             */
            createMulti = function (Model, bases) {
                var models = [];

                angular.forEach(bases, function (base) {
                    this.push(
                        createOne(Model, base)
                    );
                }, models);

                return models;
            };

        this.$get = function () {
            return {
                /**
                 * Instanciation de un ou plusieurs models
                 *
                 * @param  {Object} Model Constructeur du model
                 * @param  {Object||Array} base  Données de base
                 * @return {Object||Array}       Instance(s) du model(s)
                 */
                create: function (Model, base) {
                    var create = angular.isArray(base) ? createMulti : createOne;

                    return create(Model, base);
                }
            };
        };
    });
}));
